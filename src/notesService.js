const { Note } = require('./models/Note.js');

const getUserNotes = async (req, res) => {
  let { offset, limit } = req.query;

  await Note.find({ userId: req.user.userId })
    .then((some) => {
      if (offset === undefined) {
        offset = 0;
      } else if (limit === undefined) {
        limit = some.length;
      }
      return res.status(200).send(
        {
          offset,
          limit,
          count: some.length,
          notes: some,
        },
      );
    });
};

const addUserNotes = async (req, res) => {
  const { text } = req.body;
  if (text) {
    await Note.create({
      userId: req.user.userId,
      completed: false,
      text,
      createdDate: new Date(),
    });
    return res.status(200).send({ message: 'Success' });
  }
  return res.status(400).send({ message: 'You need to pass text to your note' });
};

const getUserNoteById = async (req, res) => {
  const noteId = req.params.id;

  if (noteId) {
    const note = await Note.findById({ _id: req.params.id, userId: req.user.userId });
    return res.status(200).send({ note });
  }
  return res.status(400).send({ message: 'You need to pass id of your note' });
};

const updateUserNoteById = async (req, res) => {
  const { text } = req.body;

  if (text !== undefined) {
    await Note.findByIdAndUpdate(
      { _id: req.params.id, userId: req.user.userId },
      { $set: { text: req.body.text } },
    );
    return res.status(200).send({ message: 'Success' });
  }
  return res.status(400).send({ message: 'You need to pass text to change your note' });
};

async function toggleCompletedForUserNoteById(req, res) {
  const note = await Note.findById({ _id: req.params.id, userId: req.user.userId });
  if (note.completed === true) {
    await Note.findByIdAndUpdate(
      { _id: req.params.id, userId: req.user.userId },
      { $set: { completed: false } },
    );
    return res.status(200).json({ message: 'Note was marked not completed' });
  }
  await Note.findByIdAndUpdate(
    { _id: req.params.id, userId: req.user.userId },
    { $set: { completed: true } },
  );
  return res.status(200).json({ message: 'Note was marked completed' });
}

const deleteUserNoteById = async (req, res) => {
  const note = await Note.findById({ _id: req.params.id, userId: req.user.userId });
  if (note) {
    await Note.findByIdAndDelete({ _id: req.params.id, userId: req.user.userId });
    return res.status(200).send({ message: 'Success' });
  }
  return res.status(400).send({ message: 'Something went wrong' });
};

module.exports = {
  getUserNotes,
  addUserNotes,
  getUserNoteById,
  updateUserNoteById,
  toggleCompletedForUserNoteById,
  deleteUserNoteById,
};
