const express = require('express');

const router = express.Router();
const {
  getUserNotes,
  addUserNotes,
  getUserNoteById,
  updateUserNoteById,
  toggleCompletedForUserNoteById,
  deleteUserNoteById,
} = require('./notesService.js');

router
  .route('/')
  .get(getUserNotes)
  .post(addUserNotes);

router
  .route('/:id')
  .get(getUserNoteById)
  .put(updateUserNoteById)
  .patch(toggleCompletedForUserNoteById)
  .delete(deleteUserNoteById);

module.exports = {
  notesRouter: router,
};
