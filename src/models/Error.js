const mongoose = require('mongoose');

const errorSchema = mongoose.Schema({
  message: {
    type: String,
    required: true,
  },
});

const Error = mongoose.model('error', errorSchema);

module.exports = {
  Error,
};
