require('dotenv').config();

const express = require('express');
const morgan = require('morgan');
const cors = require('cors');

const app = express();
const mongoose = require('mongoose');

mongoose.connect(process.env.MONGO_URI);

const { authMiddleware } = require('./middleware/authMiddleware.js');
const { authRouter } = require('./authRouter.js');
const { usersRouter } = require('./usersRouter.js');
const { notesRouter } = require('./notesRouter.js');

app.use(cors());
app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', authMiddleware, usersRouter);
app.use('/api/notes', authMiddleware, notesRouter);

const start = async () => {
  try {
    app.listen(process.env.PORT);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
function errorHandler(err, req, res) {
  console.error(err);
  res.status(500).send({ message: 'Server error' });
}

app.use(errorHandler);
