const bcrypt = require('bcryptjs');
const { User } = require('./models/User.js');
const { Credentials } = require('./models/Credentials.js');

const getProfileInfo = async (req, res) => {
  const someUser = await User.findOne({ username: req.user.username });
  const date = new Date(parseInt(req.user.createdDate, 10) * 1000);
  if (someUser) {
    return res.status(200).send({
      user: {
        _id: req.user.userId,
        username: someUser.username,
        createdDate: date,
      },
    });
  }
  return res.status(400).send({ message: 'Something went wrong' });
};

const deleteProfile = async (req, res) => {
  const someUserCred = await Credentials.findOne({ username: req.user.username });
  const someUser = await User.findOne({ username: req.user.username });

  if (someUserCred) {
    await someUserCred.delete()
      .then(await someUser.delete());
    return res.status(200).send({ message: 'Success' });
  }
  return res.status(400).send({ message: 'Something went wrong' });
};

const changeProfilePassword = async (req, res) => {
  const { oldPassword, newPassword } = req.body;
  const someUser = await Credentials.findOne({ username: req.user.username });
  const salt = bcrypt.genSaltSync(10);

  if (bcrypt.compare(String(oldPassword), String(someUser.password))) {
    someUser.password = await bcrypt.hash(newPassword, salt);
    await someUser.save();
    return res.status(200).send({ message: 'Success' });
  }
  return res.status(400).send({ message: 'Wrong old password' });
};

module.exports = {
  getProfileInfo,
  deleteProfile,
  changeProfilePassword,
};
