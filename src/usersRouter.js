const express = require('express');

const router = express.Router();
const { getProfileInfo, deleteProfile, changeProfilePassword } = require('./usersService.js');

router
  .route('/me')
  .get(getProfileInfo)
  .delete(deleteProfile)
  .patch(changeProfilePassword);

module.exports = {
  usersRouter: router,
};
