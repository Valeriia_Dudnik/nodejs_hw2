/* eslint-disable no-else-return */
require('dotenv').config();

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User } = require('./models/User.js');
const { Credentials } = require('./models/Credentials.js');

const createProfile = async (req, res) => {
  const { username, password } = req.body;

  if (!username || !password) {
    return res.status(400).send({ message: 'You need to pass username and password' });
  } else {
    const oldUser = await Credentials.findOne({ username: req.body.username });

    if (oldUser) {
      return res.status(400).send({ message: 'User already exists' });
    } else {
      const user = await Credentials.create({
        username,
        password: await bcrypt.hash(password, 10),
      });
      await User.create({
        username: user.username,
        password: user.password,
        createdDate: new Date().toString(),
      });
    }
    return res.status(200).send({ message: 'Success' });
  }
};

const login = async (req, res) => {
  const user = await Credentials.findOne({ username: req.body.username });

  if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
    const payload = { username: user.username, userId: user._id };
    const jwtToken = jwt.sign(payload, process.env.JWT_KEY);
    return res.status(200).send({ message: 'Success', jwt_token: jwtToken });
  }
  return res.status(400).send({ message: 'Not authorized' });
};

module.exports = {
  createProfile,
  login,
};
